#include <iostream>
#include <cmath>
#include <GL/glut.h>
#include <stdlib.h>
#include <string>
#include <sstream>
//#include <unistd.h>

#include "vec.h"
#include "line.h"
#include "nangle.h"

// количество вершин
int verticesCount;
// массив вершин
VEC* vertices;
// центр(средняя точка между всеми введенными)
VEC center;
// выбранная точка для показа информации о ней
VEC selectedPoint;
// самое большое расстояние по оси Оу или Ох до вершин(для создания квадратной область отсечения)
double longgestWay = 0;

// Ширина окна, а так как это квадратное окно, то и высота ей равна
int windowsWidth = 400;
int windowsHeight = windowsWidth;

// Возвращает минимальный угол отновительно оси Ох, при это он получается по модулю меньше 90 градусов
double GetCurrentAngle(VEC _vec)
{
	double angle = VEC::GetAngle(_vec, VEC(1, 0));

	if(_vec.x == 0)
	{
		if(_vec.y > 0)
		{
			return M_PI/2;
		}
		else
		{
			return -M_PI/2;
		}
	}
	if(_vec.y == 0)
	{
		if(_vec.x > 0)
		{
			return 0;
		}
		else
		{
			return M_PI;
		}
	}
	if(_vec.x > 0)
	{
		if(_vec.y > 0)
		{
			return angle;
		}
		else
		{
			return -angle;
		}
	}
	else
	{
		if(_vec.y > 0)
		{
			return -angle;
		}
		else
		{
			return angle;
		}
	}
}
// Определенно в "nangle.h"
void NANGLE::draw(VEC _center)
{
	glBegin(GL_LINE_LOOP);
	for(int i = 0; i < this->verticesCount; i++)
	{
		glVertex2f((this->vertices[i%this->verticesCount] - center).x, (this->vertices[i%this->verticesCount] - center).y);
	}
	glEnd();
}
// Рисует окружность
// _radius - радиус окружности
// _center - центр окружности 
void DrawCircle(float _radius, VEC _center)
{
	glBegin(GL_LINE_LOOP);
	for(int i = 0; i < 360; i++)
	{
		glVertex2f(cos(i*M_PI/180)*_radius + _center.x, sin(i*M_PI/180)*_radius + _center.y);
	}
	glEnd();
}
// Рисует накресет лежащие разделяющие линии
// _nangle - 4х угольник, диагонали которого надо нарисовать
void DrawDivLines(NANGLE _nangle)
{
	glColor3f(0.0, 1.0, 0.0);
	glBegin(GL_LINES);
	for(int i = 0; i <= _nangle.VerticesCount(); i++)
	{
		glVertex2f((_nangle[i%_nangle.VerticesCount()] - center).x, (_nangle[i%_nangle.VerticesCount()] - center).y);
		glVertex2f((_nangle[(i + 2)%_nangle.VerticesCount()] - center).x, (_nangle[(i + 2)%_nangle.VerticesCount()] - center).y);
	}
	glEnd();
}
// Рисует дугу, хоть и называет это радугой)
// _radius - радиус дуги
// _width - толщина линии дуги
// _center - центр дуги
// _angleBegin - угол начала дуги
// _angleEnd - угол конца дуги 
// (!) функция сама определяет откуда надо начинать отрисовку с конца или с начала
void DrawRainbow(float _radius, float _width, VEC _center, double _angleBegin, double _angleEnd)
{
	glBegin(GL_QUADS);

	if(_angleEnd < _angleBegin)
	{
		double buffer = _angleEnd;
		_angleEnd = _angleBegin;
		_angleBegin = buffer;
	}

	for(double d = _angleBegin; d < _angleEnd; d += (_angleEnd - _angleBegin)/100.0)
	{
		glVertex2f(cos(d)*_radius + _center.x, 
			sin(d)*_radius + _center.y);
		glVertex2f(cos(d)*(_radius + _width) + _center.x, 
			sin(d)*(_radius + _width) + _center.y);
		glVertex2f(cos(d + (_angleEnd - _angleBegin)/100.0)*(_radius + _width) + _center.x, 
			sin(d + (_angleEnd - _angleBegin)/100.0)*(_radius + _width) + _center.y);
		glVertex2f(cos(d + (_angleEnd - _angleBegin)/100.0)*_radius + _center.x, 
			sin(d + (_angleEnd - _angleBegin)/100.0)*_radius + _center.y);
	}

	glEnd();
}
void display (void)
{
	NANGLE n(verticesCount, vertices);
	NANGLE divLines = n.SearchTwoDivLines();
	VEC divLinesUnionPoint = LINE::UnionPoint(LINE(divLines[0], divLines[2]), LINE(divLines[1], divLines[3])); 
	
	glClear(GL_COLOR_BUFFER_BIT);

	glColor3f(0, 0, 1.0);
	glBegin(GL_LINES);
		glVertex2f(divLinesUnionPoint.x - (longgestWay*1.1) - center.x, divLinesUnionPoint.y - center.y);
		glVertex2f(divLinesUnionPoint.x + (longgestWay*1.1) - center.x, divLinesUnionPoint.y - center.y);
		glVertex2f(divLinesUnionPoint.x - center.x, divLinesUnionPoint.y - (longgestWay*1.1) - center.y);
		glVertex2f(divLinesUnionPoint.x - center.x, divLinesUnionPoint.y + (longgestWay*1.1) - center.y);
	glEnd();

	DrawDivLines(divLines);

	// Rainbows - begin
	double angleEnd = GetCurrentAngle(LINE(divLines[0], divLines[2]).GetVector());
	if(fabs(angleEnd) > M_PI/2)
	{
		angleEnd = GetCurrentAngle(LINE(divLines[2], divLines[0]).GetVector());
	}
	double angleBegin = angleEnd;

	glColor3f(1.0, 1.0, 0.0);
	DrawRainbow((2*longgestWay)/100*5/2, (2*longgestWay)/100*5/2, divLinesUnionPoint - center, 0, angleEnd);
	
	glColor3f(1.0, 1.0/256*67 , 0.0);
	angleEnd = GetCurrentAngle(LINE(divLines[1], divLines[3]).GetVector());	

	if(fabs(angleEnd) > M_PI/2)
	{
		angleEnd = GetCurrentAngle(LINE(divLines[3], divLines[1]).GetVector());
	}
	else
	{
		angleEnd = GetCurrentAngle(LINE(divLines[1], divLines[3]).GetVector());
	}

	DrawRainbow((2*longgestWay)/100*10/2, (2*longgestWay)/100*5/2, divLinesUnionPoint - center, 0, angleEnd);

	if(fabs(angleEnd) + fabs(angleBegin) > M_PI/2)
	{
		if(angleEnd < angleBegin)
		{
			if(angleBegin > 0)
			{
				angleEnd = angleBegin + (M_PI - (fabs(angleEnd) + fabs(angleBegin)));
			}
			else
			{
				angleEnd = angleBegin - (M_PI - (fabs(angleEnd) + fabs(angleBegin)));
			}
		}
		else
		{
			angleEnd = angleBegin - (M_PI - (fabs(angleEnd) + fabs(angleBegin)));
		}
		
	}

	glColor3f(0.0, 1.0, 0.0);
	DrawRainbow(0, (2*longgestWay)/100*5/2, divLinesUnionPoint - center, angleBegin, angleEnd);
	// Rainbows - end

	glColor3f(1.0, 0.0, 0.0);
	n.draw(center);

	glColor3f(0.5, 0.5, 1);
	DrawCircle((2*longgestWay)/100*5/2, selectedPoint - center);

	glFlush();
}
void keyboard(unsigned char key, int x, int y)
{
	if(key == 'q')
	{
		exit(0);
	}
}
// Написал для того, чтобы при приближении к определенной вершине в заголовке окна появлялись координаты "выбранной" вершины
void mouse(int x, int y)
{
	VEC windowCenter(windowsWidth/2, windowsHeight/2);
	VEC currentPointerPosition = VEC((x - windowCenter.x)/(windowsWidth/2) * (longgestWay), 
		-(y - windowCenter.y)/(windowsWidth/2) * (longgestWay)) + center;

	NANGLE currentNangle = NANGLE(verticesCount, vertices);
	NANGLE CNDivLines = currentNangle.SearchTwoDivLines();
	VEC CNDivLinesUnionPoint = LINE::UnionPoint(LINE(CNDivLines[0], CNDivLines[2]), LINE(CNDivLines[1], CNDivLines[3]));

	double smallestDistance = VEC::Distance(selectedPoint, currentPointerPosition);

	if(VEC::Distance(CNDivLinesUnionPoint, currentPointerPosition) < (2*longgestWay)/100*10/2 && 
		VEC::Distance(CNDivLinesUnionPoint, currentPointerPosition) < smallestDistance)
	{
		selectedPoint = CNDivLinesUnionPoint;
	}

	for(int i = 0; i < CNDivLines.VerticesCount(); i ++)
	{
		if(VEC::Distance(CNDivLines[i], currentPointerPosition) < (2*longgestWay)/100*10/2 && 
			VEC::Distance(CNDivLines[i], currentPointerPosition) < smallestDistance)
		{
			selectedPoint = CNDivLines[i];
			smallestDistance = VEC::Distance(selectedPoint, currentPointerPosition);
		}
	}
	for(int i = 0; i < verticesCount; i++)
	{
		if(VEC::Distance(vertices[i], currentPointerPosition) < (2*longgestWay)/100*10/2 && 
			VEC::Distance(vertices[i], currentPointerPosition) < smallestDistance)
		{
			selectedPoint = vertices[i];
			smallestDistance = VEC::Distance(selectedPoint, currentPointerPosition);
		}
	}
	/*
	char* newTitle  = new char[strlen(std::to_string(selectedPoint.x).c_str()) + 
		strlen(std::to_string(selectedPoint.y).c_str()) + 2];
	
	memcpy(newTitle, std::to_string(selectedPoint.x).c_str(), 
		strlen(std::to_string(selectedPoint.x).c_str()));
	newTitle[strlen(std::to_string(selectedPoint.x).c_str())] = ' ';

	memcpy(newTitle + strlen(std::to_string(selectedPoint.x).c_str()) + 1, 
		std::to_string(selectedPoint.y).c_str(), 
		strlen(std::to_string(selectedPoint.y).c_str()) + 1);
	glutSetWindowTitle(newTitle);

	display();

	delete [] newTitle;*/


	std::ostringstream xString, yString;
	xString << selectedPoint.x, yString << selectedPoint.y;
	std::string title = xString.str() + std::string(" ") + yString.str();

	glutSetWindowTitle(title.c_str());
	
	display();
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
int main(int argc, char **argv)
{
	// Сначала вводим число - количество вершин
	// Зачет через пробел(или как удобно) вводим координаты вершин
	// Желательно, чтобы ввод вершин оуществлялся по ходу часовой стрелки
	std::cin >> verticesCount;
	vertices = new VEC[verticesCount];
	double x = 0, y = 0;
	for(int i = 0; i < verticesCount; i++)
	{
		std::cin >> vertices[i].x >> vertices[i].y;
		x += vertices[i].x;
		y += vertices[i].y;
	}
	center = VEC(x / verticesCount, y / verticesCount);
	for(int i = 0; i < verticesCount; i++)
	{
		if(abs(center.x - vertices[i].x) > longgestWay)
			longgestWay = abs(center.x - vertices[i].x);
		if(abs(center.y - vertices[i].y) > longgestWay)
			longgestWay = abs(center.y - vertices[i].y);
	}
	longgestWay += 1;

	NANGLE l = NANGLE(verticesCount, vertices).SearchTwoDivLines();
	selectedPoint = LINE::UnionPoint(LINE(l[0], l[2]), LINE(l[1], l[3]));

	//selectedPoint.Show();
	std::cout << "__________________________________________________________________"<< std::endl;
	std::cout << "S = " << NANGLE(verticesCount, vertices).Space() << std::endl;
	std::cout << "Green  = " << VEC::GetAngle(LINE(l[0], l[2]).GetVector(), LINE(l[1], l[3]).GetVector())*180/M_PI << "g"<< std::endl;
	std::cout << "Yellow  = " << VEC::GetAngle(LINE(l[0], l[2]).GetVector(), VEC(1, 0))*180/M_PI << "g"<< std::endl;
	std::cout << "Orange  = " << VEC::GetAngle(LINE(l[1], l[3]).GetVector(), VEC(1, 0))*180/M_PI << "g"<< std::endl;
	std::cout << "__________________________________________________________________"<< std::endl;

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(windowsWidth, windowsHeight);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Test");
	glClearColor(0.0, 0.0, 0.0, 0.0);

	glOrtho(-longgestWay, longgestWay, -longgestWay, longgestWay, -1, 1);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glutKeyboardFunc(keyboard);
	glutPassiveMotionFunc(mouse);
	// чтобы размер окна оставался неизменным
	glutReshapeFunc([](int width, int height){glutReshapeWindow(windowsWidth, windowsHeight);display();});
	
	display();

	glutMainLoop();
	return 0;
}