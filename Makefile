all: build
build: main.cpp vec.o line.o triangle.o nangle.o freeglut
	g++ main.cpp vec.o line.o triangle.o nangle.o -std=c++11 -o main -lGL -lglut
nangle.o: nangle.cpp triangle.o line.o vec.o
	g++ -c nangle.cpp -std=c++11
triangle.o: triangle.cpp
	g++ -c triangle.cpp -std=c++11
line.o: vec.o line.cpp
	g++ -c line.cpp -std=c++11
vec.o: vec.cpp 
	g++ -c vec.cpp -std=c++11
freeglut:
	sudo apt-get -y install freeglut3-dev
test: nangle build
	./main < nangle.txt
	rm *.txt
nangle: 
	echo "5 0 3 3 4 4 2 3 0 1 0" >> nangle.txt