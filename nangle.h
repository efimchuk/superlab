#pragma once

#include "vec.h"
#include "line.h"

// Класс для работы с многоугольниками, заданными количеством вершин и, собственно, координатами этих вершин
class NANGLE
{
	int verticesCount;	// Количество вершин
	VEC* vertices;		// Массив вершин
	// Вычисляет прямую проходящую через данную точку и делящую многоугольник на два равновеликих
	// _point - данная точка
	LINE SearchDivLine(VEC _point);

	public:
	// Конструктор
	// _verticesCount 	- количество вершин
	// _vertices 		- массив вершин
	NANGLE(int _verticesCount, const VEC * _vertices);

	// Конструктор копирования
	// _nAngle - объект, который надо скопировать
	NANGLE(const NANGLE & _nAngle);

	// Деструктор нужен, так как мы имеем дело с выделением памяти внутри класса =)
	~NANGLE();

	// Возвращает количество вершин многоугольника 
	int VerticesCount();

	// Выводит воодринаты всех вершин треугольника в формате "(х, у)-(x, y)-...-(х, у)"
	void Show();

	// Возвращает площадь данного многоугольника
	double Space();

	// Возвращает количество вершин многоугольника, которые лежат слева от заданной прямой
	// _line - задаваемая прямая
	int VerticesLeftOfTheLineCount(LINE _line);

	// Возвращает количество вершин многоугольника, которые лежат справа от заданной прямой
	// _line - задаваемая прямая
	int VerticesRightOfTheLineCount(LINE _line);

	// Возвращает количество вершин многоугольника, которые лежат на заданной прямой
	// _line - задаваемая прямая
	int VerticesOnTheLineCount(LINE _line);

	// Возвращает отрезок прямой, внутри многоугольника
	// (!) Если результатом станет прямая описанная двумя одинаковыми точками, 
	//		то либо прямая лишь касается одной вершины либо вообще не проходит через многоугольник
	// _line - заданая прямая
	LINE UnionPoints(LINE _line);

	// Возвращает (расположенный слева от прямой) многоугольник, образованный делением заданной прямой текущего многольника
	// _line - заданая прямая
	NANGLE NangleLeftOfTheLine(LINE _line);

	// Возвращает (расположенный справа от прямой) многоугольник, образованный делением заданной прямой текущего многольника
	// _line - заданая прямая
	NANGLE NangleRightOfTheLine(LINE _line);

	// Вычисляет отрезок(прямую), исходящую из заданной вершины и делящую многоугольник на две равные(по площади) части
	// _pointIndex - индекс вершины в массиве вершин
	LINE SearchDivLine(int _pointIndex);

	// Вычисляет две линии, которые пересекаясь делят фигуру на 4е равные по площади. Возвращает 4х-угольник, диагонали которого и есть наши разделяющие линии
	NANGLE SearchTwoDivLines();

	// Рисует многоугольник, так, чтобы казалось, что он рисует его вокруг начала координат, задается "центр"(только для того, чтобы рисовать красиво)
	void draw(VEC _center);

	// Возвращает вершину с заданным индексом в массиве
	// _index - индекс требуемой вершины
	VEC& operator[](int _index);

	// Конструктор присваивания
	NANGLE operator=(const NANGLE & _nAngle);

	// Определеяет находится ли точка в текущем многоугольнике
	// _point - даннная точка
	bool IsIn(VEC _point);
};