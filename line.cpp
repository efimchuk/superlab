#include <stdio.h>
#include <string.h>
#include <iostream>
#include <cmath>

#include "line.h"

LINE::LINE()
{
	this->vertices = new VEC[2];
}
LINE::LINE(VEC _vec1, VEC _vec2)
{
	this->vertices = new VEC[2];
	this->vertices[0] = _vec1;
	this->vertices[1] = _vec2;
}
LINE::LINE(const LINE & _line)
{	
	this->vertices = new VEC[2];
	this->vertices[0] = _line.vertices[0];
	this->vertices[1] = _line.vertices[1];
}
LINE::~LINE()
{
	delete [] this->vertices;
}
void LINE::Show()
{
	for(int i = 0; i < 2; i++)
	{
		printf("(%.*f, %.*f", 2, this->vertices[i].x, 2, this->vertices[i].y);
	}
	std::cout << std::endl;
}
double LINE::Length() 
{
	return VEC::Distance(this->vertices[0], this->vertices[1]);
}
VEC LINE::GetVector() 
{
	return VEC::GetVector(this->vertices[0], this->vertices[1]);
}
int LINE::WhereIsPoint(VEC _point)
{
	// используем матричный способ определения положения точки относительно прямой

	#define a this->vertices[0].x
	#define b this->vertices[0].y
	#define c this->vertices[1].x
	#define d this->vertices[1].y
	#define e _point.x
	#define f _point.y

	// определитель матрицы
	//	|a b 1|
	//	|c d 1|
	//	|e f 1|
	double result = (a*d + c*f + b*e - d*e - f*a - b*c);
	
	// Проблемы сравнения чисел double на равенство
	return ((fabs(result - 0) > 0.000001) ? (result > 0.0f ? 1 : -1) : 0);
}
VEC LINE::UnionPoint(LINE _A, LINE _B) 
{
	// вектор прямой А
	VEC Av = _A.GetVector();
	// вектор прямой В
	VEC Bv = _B.GetVector();
	// точка пересечения, которую еще предстоит вычислить
	VEC result;

	// Следующие две строки мы получили составив общее уравнение прямой на плоскости "Ax + By + C = 0"
	// и выразив х(у) приравняли его для того, чтобы узнать общий y(x) двух прямых
	result.x = (Av.x*Bv.x*(_A[0].y - _B[0].y) + Av.x*Bv.y*_B[0].x - Bv.x*Av.y*_A[0].x)/(Av.x*Bv.y - Bv.x*Av.y);
	result.y = (Av.y*Bv.y*(_B[0].x - _A[0].x) + Bv.y*Av.x*_A[0].y - Av.y*Bv.x*_B[0].y)/(Bv.y*Av.x - Av.y*Bv.x);		
	
	return result;
}
VEC& LINE::operator[](int _index)
{
	return this->vertices[_index];
}
VEC LINE::operator=(const LINE & _line)	
{
	memcpy(this->vertices, _line.vertices, sizeof(VEC)*2);
}
bool LINE::operator==(const LINE& _line)
{
	return (this->vertices[0] == _line.vertices[0]) && (this->vertices[1] == _line.vertices[1]);
}