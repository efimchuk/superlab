#include <stdio.h>
#include <string.h>
#include <iostream>
#include <cmath>

#include "triangle.h"
#include "line.h"

TRIANGLE::TRIANGLE() 
{
	this->vertices = new VEC[3];
}
TRIANGLE::TRIANGLE(VEC _p1, VEC _p2, VEC _p3)
{
	this->vertices = new VEC[3];
	this->vertices[0] = _p1;
	this->vertices[1] = _p2;
	this->vertices[2] = _p3;
}
TRIANGLE::TRIANGLE(const TRIANGLE & _triangle)
{
	this->vertices = new VEC[3];
	this->vertices[0] = _triangle.vertices[0];
	this->vertices[1] = _triangle.vertices[1];
	this->vertices[2] = _triangle.vertices[2];
}
TRIANGLE::~TRIANGLE()
{
	delete [] this->vertices;
}
void TRIANGLE::Show()
{
	for(int i = 0; i < 3; i++)
	{
		printf("(%*.*f, %*.*f) ", 3, 2, this->vertices[i].x, 3, 2, this->vertices[i].y);
	}
	std::cout << std::endl;
}
VEC& TRIANGLE::operator[](int _index)
{
	return this->vertices[_index];
}
double TRIANGLE::Space()
{
	// Для того, чтобы хоть как-то облегчить читаемость и объем кода вычислим длины сторон однажды
	#define a LINE(this->vertices[0], this->vertices[1]).Length()
	#define b LINE(this->vertices[1], this->vertices[2]).Length()
	#define c LINE(this->vertices[2], this->vertices[0]).Length()

	// Уравнение Герона для площади треугольника, зная длины всех его сторон
	return sqrt((a + b + c)*(b + c - a)*(a + c - b)*(a + b - c))/4;
}