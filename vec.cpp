#include <stdio.h>
#include <cmath>
#include <iostream>

#include "vec.h"

VEC::VEC(){}
VEC::VEC(const VEC & _vec)
{
	this->x = _vec.x;
	this->y = _vec.y;
}
VEC::VEC(double _x, double _y)
{
	this->x = _x;
	this->y = _y;
}
void VEC::Show() 
{
	printf("(%.*f, %.*f)\n", 2, this->x, 2, this->y);
}
bool VEC::operator==(const VEC & _vec)
{
	// Проблемы сравнения чисел double на равенство :(

	//return ((float)this->x == (float)_vec.x) && ((float)this->y == (float)_vec.y);
	return (fabs(this->x - _vec.x) == 0 && fabs(this->y - _vec.y)  == 0);
	//return EqualsDouble(this->x, _vec.x) && EqualsDouble(this->y, _vec.y);
}
VEC VEC::operator=(const VEC & _vec)
{
	this->x = _vec.x;
	this->y = _vec.y;

	return VEC(*this);
}	
VEC VEC::operator*(double _number)
{
	return VEC(this->x*_number, this->y*_number);
}
VEC VEC::operator+(const VEC & _vec)
{
	return VEC(this->x + _vec.x, this->y + _vec.y);
}
VEC VEC::operator-(const VEC & _vec)
{
	return VEC(this->x - _vec.x, this->y - _vec.y);
}
VEC VEC::GetVector(VEC _vec1, VEC _vec2)
{
	VEC fVec(_vec2.x - _vec1.x, _vec2.y - _vec1.y);
	fVec.x = fVec.x / VEC::Distance(_vec1, _vec2);
	fVec.y = fVec.y / VEC::Distance(_vec1, _vec2);

	return fVec;
}
double VEC::Distance(VEC _vec1, VEC _vec2)
{
	return sqrt(pow(_vec2.x - _vec1.x, 2) + pow(_vec2.y - _vec1.y, 2));
}
double VEC::GetAngle(VEC _vec1, VEC _vec2)
{
	if(fabs(acos((_vec1.x * _vec2.x) + (_vec1.y * _vec2.y) / (VEC::Distance(VEC(0, 0), _vec1) * VEC::Distance(VEC(0, 0), _vec2)))) < M_PI/2)
	{
		return acos((_vec1.x * _vec2.x) + (_vec1.y * _vec2.y) / (VEC::Distance(VEC(0, 0), _vec1) * VEC::Distance(VEC(0, 0), _vec2)));
	}
	else
	{
		return acos(((-_vec1.x) * _vec2.x) + ((-_vec1.y) * _vec2.y) / (VEC::Distance(VEC(0, 0), _vec1*(-1)) * VEC::Distance(VEC(0, 0), _vec2)));
	}
}