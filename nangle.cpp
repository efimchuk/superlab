#include <string.h>
#include <stdio.h>
#include <cmath>
#include <iostream>

#include "nangle.h"
#include "triangle.h"

NANGLE::NANGLE(int _verticesCount, const VEC * _vertices) 
{
	// if(_verticesCount < 3)
	// {
	// 	std::cout << "(!) Осторожно! Был создан точно не многоугольник. =) " << std::endl;
	// }
	// for(int i = 0; i < _verticesCount; i++)
	// {
	// 	if(LINE(_vertices[i], _vertices[(i + 2)%_verticesCount]).WhereIsPoint(_vertices[(i + 1)%_verticesCount]) == -1)
	// 	{
	// 		std::cout << "(!) Осторожно! Был создан не выпуклый многоугольник." << std::endl;
	// 	}
	// }
	this->verticesCount = _verticesCount;
	this->vertices = new VEC[_verticesCount];
	memcpy(this->vertices, _vertices, sizeof(VEC)*this->verticesCount);
}
NANGLE::NANGLE(const NANGLE& _nAngle)
{
	this->verticesCount = _nAngle.verticesCount;
	this->vertices = new VEC[this->verticesCount];
	memcpy(this->vertices, _nAngle.vertices, sizeof(VEC)*this->verticesCount);
}
NANGLE::~NANGLE()
{
	if(this->verticesCount)
	{
		if(this->verticesCount > 1)
		{
			delete [] this->vertices;
		}
		else
		{
			delete this->vertices;
		}
	}
}
int NANGLE::VerticesCount()
{
	return this->verticesCount;
}
void NANGLE::Show()
{
	for(int i = 0; i < this->verticesCount; i++)
	{
		printf("(%.*f, %.*f)", 2, this->vertices[i].x, 2, this->vertices[i].y);
	}
	std::cout << std::endl;
}
double NANGLE::Space()
{
	// используется специальная формула для нахождения площади многоугольника(есть на странице о многоугольниках в Wiki)
	double result = 0;
	for(int i = 0; i < this->verticesCount; i++)
	{
		result 	+= 	(this->vertices[i].x + this->vertices[(i + 1)%this->verticesCount].x)*
					(this->vertices[i].y - this->vertices[(i + 1)%this->verticesCount].y);
	}

	return result/2;
}
int NANGLE::VerticesLeftOfTheLineCount(LINE _line)
{
	int verticesLeftOfTheLineCount = 0;
	for (int i = 0; i < this->verticesCount; i++)
	{
		if(_line.WhereIsPoint(this->vertices[i]) == 1)
			verticesLeftOfTheLineCount ++;
	}

	return verticesLeftOfTheLineCount;
}
int NANGLE::VerticesRightOfTheLineCount(LINE _line)
{
	int verticesLeftOfTheLineCount = 0;
	for (int i = 0; i < this->verticesCount; i++)
	{
		if(_line.WhereIsPoint(this->vertices[i]) == -1)
			verticesLeftOfTheLineCount ++;
	}

	return verticesLeftOfTheLineCount;
}
int NANGLE::VerticesOnTheLineCount(LINE _line)
{
	int verticesLeftOfTheLineCount = 0;
	for (int i = 0; i < this->verticesCount; i++)
	{
		if(_line.WhereIsPoint(this->vertices[i]) == 0)
			verticesLeftOfTheLineCount ++;
	}

	return verticesLeftOfTheLineCount;
}
LINE NANGLE::UnionPoints(LINE _line)
{
	LINE result;
	bool firstPointSetted = false;
	for(int i = 0; i < this->verticesCount; i++)
	{
		if(_line.WhereIsPoint(this->vertices[i%this->verticesCount]) != _line.WhereIsPoint(this->vertices[(i + 1)%this->verticesCount])
			&& _line.WhereIsPoint(this->vertices[(i + 1)%this->verticesCount]) == 1)
		{
			result[0] = LINE::UnionPoint(_line, LINE(this->vertices[i%this->verticesCount], this->vertices[(i + 1)%this->verticesCount]));
			i = (i + 1)%this->verticesCount;
			for(; _line.WhereIsPoint(this->vertices[(i + 1)%this->verticesCount]) == 1; i++){}
			result[1] = LINE::UnionPoint(_line, LINE(this->vertices[i%this->verticesCount], this->vertices[(i + 1)%this->verticesCount]));
			return result;
		}
	}
}
NANGLE NANGLE::NangleLeftOfTheLine(LINE _line)
{
	if(this->VerticesLeftOfTheLineCount(_line))
	{
		if(this->VerticesOnTheLineCount(_line) == 1 && this->VerticesRightOfTheLineCount(_line) == 0)
		{
			return NANGLE(*this);
		}
		int newNangleVerticesCount = this->VerticesLeftOfTheLineCount(_line) + 2;
		VEC* newNangleVertices = new VEC[newNangleVerticesCount];
		newNangleVertices[0] = this->UnionPoints(_line)[0];
		newNangleVertices[newNangleVerticesCount - 1] = this->UnionPoints(_line)[1];
		for(int i = 0; i < this->verticesCount; i++)
		{
			if(_line.WhereIsPoint(this->vertices[i]) != _line.WhereIsPoint(this->vertices[(i + 1)%this->verticesCount])
				&& _line.WhereIsPoint(this->vertices[(i + 1)%this->verticesCount]) == 1)
			{
				i = (i + 1)%this->verticesCount;
				for(int newNangleVerticesIndex = 1; _line.WhereIsPoint(this->vertices[(i + newNangleVerticesIndex - 1)%this->verticesCount]) == 1; newNangleVerticesIndex++)
				{
					newNangleVertices[newNangleVerticesIndex] = this->vertices[(i + newNangleVerticesIndex - 1)%this->verticesCount];
				}

				NANGLE result(newNangleVerticesCount, newNangleVertices);
				delete [] newNangleVertices;
				return result;
			}
		}
	}
	else
	{
		return NANGLE(0, NULL);
	}
}
NANGLE NANGLE::NangleRightOfTheLine(LINE _line)
{
	if(this->VerticesRightOfTheLineCount(_line))
	{
		if(this->VerticesOnTheLineCount(_line) == 1 && this->VerticesLeftOfTheLineCount(_line) == 0)
		{
			return NANGLE(*this);
		}
		int newNangleVerticesCount = this->VerticesRightOfTheLineCount(_line) + 2;
		VEC* newNangleVertices = new VEC[newNangleVerticesCount];
		newNangleVertices[0] = this->UnionPoints(_line)[1];
		newNangleVertices[newNangleVerticesCount - 1] = this->UnionPoints(_line)[0];
		for(int i = 0; i < this->verticesCount; i++)
		{
			if(_line.WhereIsPoint(this->vertices[i]) != _line.WhereIsPoint(this->vertices[(i + 1)%this->verticesCount])
				&& _line.WhereIsPoint(this->vertices[(i + 1)%this->verticesCount]) == -1)
			{
				i = (i + 1)%this->verticesCount;
				for(int newNangleVerticesIndex = 1; _line.WhereIsPoint(this->vertices[(i + newNangleVerticesIndex - 1)%this->verticesCount]) == -1; newNangleVerticesIndex++)
				{
					newNangleVertices[newNangleVerticesIndex] = this->vertices[(i + newNangleVerticesIndex - 1)%this->verticesCount];
				}

				NANGLE result(newNangleVerticesCount, newNangleVertices);
				delete [] newNangleVertices;
				return result;
			}
		}
	}
	else
	{
		return NANGLE(0, NULL);
	}
}
LINE NANGLE::SearchDivLine(int _pointIndex)
{
	double halfSpace = this->Space()/2;
	for(int i = 0; i + 2 < this->verticesCount; i++)
	{
		double tSpace = TRIANGLE(this->vertices[_pointIndex], 
			this->vertices[(i + _pointIndex + 1)%this->verticesCount], 
			this->vertices[(i + _pointIndex + 2)%this->verticesCount]).Space();
		if((float)tSpace == (float)halfSpace)
		{
			return LINE(this->vertices[_pointIndex], this->vertices[(i + _pointIndex + 2)%this->verticesCount]);
		}
		else
		{
			if(tSpace < halfSpace)
			{
				halfSpace -= tSpace;
			}
			else
			{
				double a = (halfSpace * LINE(this->vertices[(i + _pointIndex + 1)%this->verticesCount], 
					this->vertices[(i + _pointIndex + 2)%this->verticesCount]).Length()) / tSpace;
				VEC vector = VEC::GetVector(this->vertices[(i + _pointIndex + 1)%this->verticesCount], 
					this->vertices[(i + _pointIndex + 2)%this->verticesCount]);
				VEC point = this->vertices[(i + _pointIndex + 1)%this->verticesCount] + vector*a;

				return LINE(this->vertices[_pointIndex], point);
			}
		}
	}
}
LINE NANGLE::SearchDivLine(VEC _point)
{
	for(int i = 0; i < this->verticesCount; i++)
	{
		if(LINE(this->vertices[i], this->vertices[(i + 1)%this->verticesCount]).WhereIsPoint(_point) == 0)
		{
			int lastVertexIndex = i;
			double halfSpace = this->Space()/2;
			if(_point == this->vertices[(i + 1)%this->verticesCount])
			{
				i += 2;
			}
			else
			{
				i ++;
			}
			for(; i%this->verticesCount != lastVertexIndex; i++)
			{
				double tSpace = TRIANGLE(_point, this->vertices[i%this->verticesCount], this->vertices[(i + 1)%this->verticesCount]).Space();
				if((float)tSpace == (float)halfSpace)
				{
					return LINE(_point, this->vertices[(i + 1)%this->verticesCount]);
				}
				else
				{
					if(tSpace < halfSpace)
					{
						halfSpace -= tSpace;
					}
					else
					{
						double a = (halfSpace * LINE(this->vertices[i%this->verticesCount], this->vertices[(i + 1)%this->verticesCount]).Length())/tSpace;
						VEC vector = VEC::GetVector(this->vertices[i%this->verticesCount], this->vertices[(i + 1)%this->verticesCount]);
						VEC point = this->vertices[i%this->verticesCount] + vector*a;

						return LINE(_point, point);
					}
				}
			}
			break;
		}
	}
	std::cout << "Сорянчик, но точка не лежит на периметре многоугольника. =(\n";
}
NANGLE NANGLE::SearchTwoDivLines()
{
	NANGLE currentNangle = *this;
	
	double ZOOM  = 10; // коэфициент увеличения фигуры
	/*
		Сделан потому что при маленьких размерах фигуры (маленьких длинах сторон) деление сторон производится до опеределенного 
		момента, причем этот самый момент наступает слишком рано, что вызванно проблемами с работой над маленькими значениями
		переменной типа double, что сильно влияет на точность вычисления прямых делящих фигуру на 4 равные части - эти четыре
		части очень разные (при маленьком значении этого коэфициента, при маленьких размерах самой фигуры) получаются по площади.
		Увелечивая фигуру мы не изменяем ее, значит, линии делащие ее будут лежать под таким же углом, что и линии у такой же
		фигуры размерами по меньше, а точка из пересечения будет лежать в том же месте, относительно всей фигуры (большой), что
		и для маленькой. На самом деле это можно увидеть, если приравнять коэфициенты не значение 10, а 1, собрать программу и
		вызвать ее для расчета линия для многоугольника, вершины которого лежат в файле "nangle" написав вызов "./super < nangle".
		Далее изменить значение коэфициента снова на 10 и опять вызвать программу...фигура такая же, а линии разные, причем, во
		второй раз они будут куда более приближены. Причем, как показала практика, для этого коэфициента необязательно придавать
		большие значения - 2х значного числа вполне достаточно будет, так как она не увеличивает фигуру, а умножает воординаты
		каждой вершины на себя, а таким образом, при значении коэф-та, например, "2" площадь фигуры для расчета делящих линий
		увеличится в 4 раза. По крайней мере, когда я писал программу было так, может быть к тому моменту, когда вы это читаете,
		я смог решить эту проблему иначе)
	*/
	
	for(int i = 0; i < currentNangle.verticesCount; i++)
	{
		currentNangle[i] = currentNangle[i] * ZOOM;
	}

	LINE firstLine = currentNangle.SearchDivLine(0);
	LINE secondLine;
	NANGLE nangleLeftOfTheLine = currentNangle.NangleLeftOfTheLine(firstLine);
	LINE approximationLine;
	
	for(int i = 1; i < nangleLeftOfTheLine.verticesCount - 1; i++)
	{
		secondLine = currentNangle.SearchDivLine(nangleLeftOfTheLine.vertices[i]);
		if((float)(nangleLeftOfTheLine.NangleRightOfTheLine(secondLine).Space()) == (float)(currentNangle.Space()/4))
		{
			VEC* newNangleVertices = new VEC[4];
			newNangleVertices[0] = firstLine[0] * (1/ZOOM);
			newNangleVertices[1] = secondLine[0] * (1/ZOOM);
			newNangleVertices[2] = firstLine[1] * (1/ZOOM);
			newNangleVertices[3] = secondLine[1] * (1/ZOOM);

			NANGLE newNangle(4, newNangleVertices);
			delete [] newNangleVertices;
			return newNangle;
		}
		else
		{
			if(nangleLeftOfTheLine.NangleRightOfTheLine(secondLine).Space() > currentNangle.Space()/4)
			{
				approximationLine = LINE(nangleLeftOfTheLine[i - 1], nangleLeftOfTheLine[i]);
				break;
			}
		}
	}
	
	while((float)(nangleLeftOfTheLine.NangleRightOfTheLine(secondLine).Space()) != (float)(currentNangle.Space()/4))
	//for(int i = 0; i < 1000; i++)
	{
		VEC newApproximationPoint = approximationLine[0] + approximationLine.GetVector()*(approximationLine.Length()/2);
		secondLine = currentNangle.SearchDivLine(newApproximationPoint);
		LINE currentApproximationLine = approximationLine;
		if(nangleLeftOfTheLine.NangleRightOfTheLine(secondLine).Space() > currentNangle.Space()/4)
		{
			approximationLine[1] = newApproximationPoint;
		}
		else
		{
			if((float)(nangleLeftOfTheLine.NangleRightOfTheLine(secondLine).Space()) == (float)(currentNangle.Space()/4))
			{
				VEC* newNangleVertices = new VEC[4];
				newNangleVertices[0] = firstLine[0] * (1/ZOOM);
				newNangleVertices[1] = secondLine[0] * (1/ZOOM);
				newNangleVertices[2] = firstLine[1] * (1/ZOOM);
				newNangleVertices[3] = secondLine[1] * (1/ZOOM);

				NANGLE newNangle(4, newNangleVertices);
				delete [] newNangleVertices;
				return newNangle;
			}
			approximationLine[0] = newApproximationPoint;
		}
		if(currentApproximationLine == approximationLine)
		{
			break;
		}
	}

	VEC* newNangleVertices = new VEC[4];
	newNangleVertices[0] = firstLine[0] * (1/ZOOM);
	newNangleVertices[1] = secondLine[0] * (1/ZOOM);
	newNangleVertices[2] = firstLine[1] * (1/ZOOM);
	newNangleVertices[3] = secondLine[1] * (1/ZOOM);

	NANGLE newNangle(4, newNangleVertices);
	delete [] newNangleVertices;
	return newNangle;
}
VEC & NANGLE::operator[](int _index)
{
	return this->vertices[_index];
}
NANGLE NANGLE::operator=(const NANGLE & _nAngle)
{
	if(this->verticesCount)
	{
		delete [] this->vertices;
	}
	this->verticesCount = _nAngle.verticesCount;
	this->vertices = new VEC[_nAngle.verticesCount];
	memcpy(this->vertices, _nAngle.vertices, sizeof(VEC)*this->verticesCount);
	return NANGLE(*this);
}
bool NANGLE::IsIn(VEC _point)
{
	for(int i = 0; i < this->verticesCount; i++)
	{
		if(LINE(this->vertices[i], this->vertices[(i + 1)%this->verticesCount]).WhereIsPoint(_point) == 1)
		{
			return false;
		}
	}
	return true;
}